<?php

session_start();

include_once("db.php");
include_once("globals.php");

if(!isset($_SESSION["user"])){
	header("Location: index.php");
}


if(isset($_POST["rating"])){
	$rating = $_POST["rating"];
	$data = $_POST["movie_id"];
	$json = (array) json_decode($data);
	$json["rating"] = $rating;
	$add = true;
	$i = 0;
	foreach($_SESSION["user"]["movies"] as $m){
		if($m["movie_id"] == $json["movie_id"]){
			$_SESSION["user"]["movies"][$i] = $json;
			$add = false;
			break;
		}
		$i++;
	}
	if($add){
		array_push($_SESSION["user"]["movies"], $json);
	}
}

?>

<!doctype html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<title>Movie Finder</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
	<link href="css/index.css" rel="stylesheet">
	<link rel="icon" href="img/icon.png" />
	<script src="js/movies.js"></script>
</head>
<body>
	<div class="container">
		<div class="row" align=center>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
				<a href="index.php"><button class="btn btn-primary">Déconnexion</button></a>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				<h1 align="center">Movie Finder</h1>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
				<b><?php echo $_SESSION["user"]["username"]; ?></b>
				<a href="validate.php"><button class="btn btn-primary">Envoyer</button></a>
			</div>
		</div>
		<div class="row" align=center>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" align=right>
				<a href="home.php"><button class="btn btn-primary">Accueil</button></a>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" align=left>
				<button class="btn btn-secondary">Rechercher</button>
			</div>
		</div>
		<br/>
		<div class="row" align=center>
			<div class="mb-3">
				<label for="movie" class="form-label">Rechercher des films</label>
				<input id="search" type="text" class="form-control" name="movie" placeholder="Star wars..." value=<?php if(isset($_SESSION["text"])){echo $_SESSION["text"];} ?>>
				<br/>
				<button id="searchBtn" class="btn btn-primary" type="submit">Rechercher</button>
			</div>
		</div>
		<?php if(isset($_SESSION["text"]) && strcmp($_SESSION["text"], "") !== 0){echo "<script>search();</script>";} ?>
		<br/>
		<div class="row" id="movies">
			
		</div>
	</div>
</body>
</html>