function getMovieElement(movie, showRating, showNotSeen) {
	let div = document.createElement("div");
	div.classList.add("card");
	div.id = movie["movie_id"];

	let img = document.createElement("img");
	img.classList.add("card-img-top");
	img.src = "./img/error.jpg";

	let TMDB_PHOTOS_PATH = "https://image.tmdb.org/t/p/w500/";

	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			const container = div;
			if (container) {
				const currentMoviePath = JSON.parse(this.responseText)["movie_results"][0];
				if (currentMoviePath != undefined && currentMoviePath != null) {
					if (currentMoviePath["poster_path"] != undefined && currentMoviePath["poster_path"] != null) {
						img.src = TMDB_PHOTOS_PATH + currentMoviePath["poster_path"];
					}
				}
				else {
					const currentTVPath = JSON.parse(this.responseText)["tv_results"][0];
					if (currentTVPath != undefined && currentTVPath != null) {
						if (currentTVPath["poster_path"] != undefined && currentTVPath["poster_path"] != null) {
							img.src = TMDB_PHOTOS_PATH + currentTVPath["poster_path"];
						}
					}
				}
			}
		}
	};
	xhttp.open("GET", "https://api.themoviedb.org/3/find/" + movie["movie_id"] + "?api_key=1d29f322865bdf00bfe226220835f183&external_source=imdb_id", true);
	xhttp.send();

	div.appendChild(img);

	let content = document.createElement("div");
	content.classList.add("card-body");

	let title = document.createElement("h5");
	title.classList.add("card-title");
	title.innerHTML = movie["primary_title"] + " (" + movie["start_year"] + ")";

	let rating = document.createElement("form");
	rating.classList.add("rating");
	rating.action = "";
	rating.method = "post";
	rating.setAttribute("align", "center");
	let ratingId = document.createElement("input");
	ratingId.type = "hidden";
	ratingId.name = "movie_id";
	ratingId.value = JSON.stringify(movie);
	rating.appendChild(ratingId);
	if (showRating) {
		for (let i = 1; i <= 10; i++) {
			let btn = document.createElement("button");
			btn.classList.add("btn");
			btn.classList.add("btn-primary");
			btn.value = i;
			btn.innerHTML = i;
			btn.type = "submit";
			btn.name = "rating";

			rating.appendChild(btn);
		}
		if (showNotSeen) {
			let notSeenBtn = document.createElement("button");
			notSeenBtn.classList.add("btn");
			notSeenBtn.classList.add("btn-primary");
			notSeenBtn.type = "submit";
			notSeenBtn.name = "notSeen";
			notSeenBtn.innerHTML = "Passer";
			rating.appendChild(notSeenBtn);
		}
	} else {
		let b = document.createElement("b");
		b.innerHTML = movie["rating"];
		rating.appendChild(b);

		let btn = document.createElement("button");
		btn.classList.add("btn");
		btn.classList.add("btn-danger");
		btn.innerHTML = "Delete";
		btn.type = "submit";
		btn.name = "delete";
		rating.appendChild(btn);
    }


	content.appendChild(title);
	content.appendChild(rating);

	div.appendChild(content);
	return div;
}

function searchText(text, offset, limit, showNotSeen) {
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			const moviesContainer = document.getElementById("movies");
			moviesContainer.innerHTML = "";
			console.log(this.responseText);
			const movies = JSON.parse(this.responseText);
			for (let movie of movies) {
				moviesContainer.appendChild(getMovieElement(movie, true, showNotSeen));
			}
		}
	};
	xhttp.open("GET", "searchMovie.php?text=" + text + "&offset=" + offset + "&limit=" + limit, true);
	xhttp.send();
}

function search() {
	const text = document.getElementById("search").value;
	if (text != "") {
		searchText(text, 0, 10, false);
	}
}

window.addEventListener("load", function () {
	let searchBtn = document.getElementById("searchBtn");
	if (searchBtn) {
		searchBtn.addEventListener("click", search);
	}
	let searchInput = document.getElementById("search");
	if (searchInput) {
		searchInput.addEventListener("keypress", this.onKeyPress);
	}
});

function onKeyPress(e) {
	if (e.keyCode == 13) {
		search();
	}
}