<?php

session_start();

include_once("db.php");
include_once("globals.php");

$error = "";

unset($_SESSION["user"]);
if(isset($_POST["register"])){
	$username = $_POST["username"];

	$sql = "SELECT * FROM users WHERE username='" . pg_escape_string($username) . "' LIMIT 1";
	$res = query($trainingDb, $sql);
	if(pg_num_rows($res) > 0){
		$error = "Ce nom d'utilisateur est déjà utilisé.";
	}else{
		if(strcmp($username, "") !== 0){
			$_SESSION["user"] = array("username" => $username, "movies" => array());
			header("Location: home.php");
		}
		else{
			$error = "Veuillez entrer un nom d'utilisateur.";
		}
	}
}


?>

<!doctype html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<title>Movie Finder</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
	<link href="css/index.css" rel="stylesheet">
	<link rel="icon" href="img/icon.png" />
</head>
<body>
	<div class="container">
		<div class="row">
			<h1 align="center">Movie Finder</h1>
		</div>
		<div class="row" align=center>
			<div class="col-lg-3 col-md-3 col-sm-1 col-xs-1"></div>
			<div class="col-lg-6 col-md-6 col-sm-10 col-xs-10" align=center>
				<form method="post" action="index.php">
					<div class="mb-3">
					<label for="username" class="form-label">Nom d'utilisateur</label>
					<input type="text" class="form-control" name="username" placeholder="Jean michel...">
					</div>
					<button type="submit" class="btn btn-primary" name="register">Participer</button>
				</form>
				<span color="red"><?php echo $error; ?></span>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-1 col-xs-1"></div>
		</div>
		<div class="row">

		</div>
	</div>
</body>
</html>