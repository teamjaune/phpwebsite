<?php

session_start();

include_once("db.php");
include_once("globals.php");

if(!isset($_SESSION["user"])){
	header("Location: index.php");
}

if(isset($_POST["delete"])){
	$data = $_POST["movie_id"];
	$json = (array) json_decode($data);
	for($i = 0; $i < count($_SESSION["user"]["movies"]); $i++){
		if(isset($_SESSION["user"]["movies"][$i])){
			$element = $_SESSION["user"]["movies"][$i];
			if(strcmp($element["movie_id"], $json["movie_id"]) === 0){
				$_SESSION["user"]["movies"][$i]["rating"] = -1;
			}
		}
	}
}

if(isset($_POST["validate"])){
	$sql = "INSERT INTO users (username) VALUES('" . pg_escape_string($_SESSION["user"]["username"]) . "')";
	$res = query($trainingDb, $sql);
	if($res){
		$sql = "SELECT id FROM users WHERE username='" . pg_escape_string($_SESSION["user"]["username"]) . "'";
		$res = query($trainingDb, $sql);
		if($res){
			while ($row = pg_fetch_assoc($res)) {
				$id = $row["id"];
				$values = "";
				foreach($_SESSION["user"]["movies"] as $movie){
					if($movie["rating"] > 0){
						$values = $values . "('" . pg_escape_string($movie["movie_id"]) . "','" . pg_escape_string($id) . "','" . $movie["rating"] . "'),";
					}
				}
				if(strcmp($values, "") !== 0){
					$values = substr($values, 0, -1);
				}
				$sql = "INSERT INTO movies (movie_id, user_id, rating) VALUES " . $values;
				$res = query($trainingDb, $sql);
				if($res){
					header("Location: index.php");
				}
				break;
			}
		}
	}
}

?>

<!doctype html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<title>Movie Finder</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
	<link href="css/index.css" rel="stylesheet">
	<link rel="icon" href="img/icon.png" />
	<script src="js/movies.js"></script>
</head>
<body>
	<div class="container">
		<div class="row" align=center>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
				<a href="index.php"><button class="btn btn-primary">Déconnexion</button></a>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				<h1 align="center">Movie Finder</h1>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
				<b><?php echo $_SESSION["user"]["username"]; ?></b>
				<button class="btn btn-secondary">Envoyer</button>
			</div>
		</div>
		<div class="row" align=center>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" align=right>
				<a href="home.php"><button class="btn btn-primary">Accueil</button></a>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" align=left>
				<a href="movies.php"><button class="btn btn-primary">Rechercher</button></a>
			</div>
		</div>
		<br/>
		<div class="row" align=center>
			<form action="" method="post">
				<button class="btn btn-primary" type="submit" name="validate">Valider mes choix</button>
			</form>
		</div>
		<br/>
		<div class="row" id="movies">
			<?php
			foreach($_SESSION["user"]["movies"] as $movie){
				if($movie["rating"] > 0){
					echo "<script>document.getElementById('movies').appendChild(getMovieElement(" . json_encode($movie) . ", false, false));</script>";
				}
			}
			?>
		</div>
	</div>
</body>

</html>