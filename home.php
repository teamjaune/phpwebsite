<?php

session_start();

include_once("db.php");
include_once("globals.php");

if(!isset($_SESSION["user"])){
	header("Location: index.php");
}

if(!isset($_SESSION["offset"])){
	$_SESSION["offset"] = 0;
}

if(isset($_POST["rating"])){
	$rating = $_POST["rating"];
	$data = $_POST["movie_id"];
	$json = (array) json_decode($data);
	$json["rating"] = $rating;
	$insert = true;
	foreach($_SESSION["user"]["movies"] as $m){
		if(strcmp($m["movie_id"], $json["movie_id"]) === 0){
			$insert = false;
			break;
		}
	}
	if($insert){
		array_push($_SESSION["user"]["movies"], $json);
		$_SESSION["offset"] = $_SESSION["offset"] + 1;
	}
}
else if(isset($_POST["notSeen"])){
	$_SESSION["offset"] = $_SESSION["offset"] + 1;
}

?>

<!doctype html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<title>Movie Finder</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
	<link href="css/index.css" rel="stylesheet">
	<link rel="icon" href="img/icon.png" />
	<script src="js/movies.js"></script>
</head>
<body>
	<div class="container">
		<div class="row" align=center>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
				<a href="index.php"><button class="btn btn-primary">Déconnexion</button></a>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				<h1 align="center">Movie Finder</h1>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
				<b><?php echo $_SESSION["user"]["username"]; ?></b>
				<a href="validate.php"><button class="btn btn-primary">Envoyer</button></a>
			</div>
		</div>
		<br/>
		<div class="row" align=center>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" align=right>
				<button class="btn btn-secondary">Accueil</button>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" align=left>
				<a href="movies.php"><button class="btn btn-primary">Rechercher</button></a>
			</div>
		</div>
		<br/>
		<input type="hidden" id="search" value=""/>
		<div class="row" id="movies">
			<script>window.addEventListener("load", function () {searchText("", <?php echo $_SESSION["offset"]; ?>, 1, true);});</script>
		</div>
	</div>
</body>
</html>