<?php

session_start();

include_once("db.php");
include_once("globals.php");

if(isset($_GET["text"])){

	$text = $_GET["text"];
	$_SESSION["text"] = $text;
	$offset = isset($_GET["offset"]) ? $_GET["offset"] : 0;
	$limit = isset($_GET["limit"]) ? $_GET["limit"] : 10;

	$sql = "SELECT * FROM movies WHERE primary_title ILIKE '%" . pg_escape_string($text) . "%' ORDER BY number_votes DESC, average_rating DESC LIMIT " . pg_escape_string($limit)  . " OFFSET " . pg_escape_string($offset);
	$res = query($movieDb, $sql);
	if($res){
		$movies = array();
		while ($row = pg_fetch_assoc($res)) {
			$movie_id = $row["movie_id"];
			$continue = false;
			foreach($_SESSION["user"]["movies"] as $movie){
				if(isset($movie["rating"]) && $movie["rating"] > 0){
					if(strcmp($movie["movie_id"], $movie_id) === 0){
						$continue = true;
						continue;
					}
				}
			}
			if($continue){
				continue;
			}
			array_push($movies, array("movie_id" => $row["movie_id"], "title_type" => $row["title_type"], "primary_title" => $row["primary_title"], "start_year" => $row["start_year"], "average_rating" => $row["average_rating"], "number_votes" => $row["number_votes"]));
		}
		echo json_encode($movies);
	}
}
else if(isset($_GET["id"])){
	$movie_id = $_GET["id"];
	$sql = "SELECT * FROM movies WHERE movie_id='" . pg_escape_string($movie_id) . "' LIMIT 1";
	$res = query($movieDb, $sql);
	if($res){
		$movies = array();
		while ($row = pg_fetch_assoc($res)) {
			array_push($movies, array("movie_id" => $row["movie_id"], "title_type" => $row["title_type"], "primary_title" => $row["primary_title"], "start_year" => $row["start_year"], "average_rating" => $row["average_rating"], "number_votes" => $row["number_votes"]));
		}
		echo json_encode($movies);
	}
}

?>